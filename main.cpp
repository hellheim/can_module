#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/can.h>
#include <linux/can/bcm.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "header/config_can.hpp"
#include "header/read_can_frame.hpp"
#include "header/write_can_frame.hpp"


using namespace std;

int main(int argc, char** argv)
{
    int s,n,opc;
    config_struct_can config_;
	param_struct param_;
    config_can(&config_, &s,"can0",RAW_MODE);
	read_can_frame(s,&param_,&opc);
	write_can_frame(s,&param_,OPC_UPDATE);
    return 0;
}